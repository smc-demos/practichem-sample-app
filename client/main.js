import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { ReactiveDict } from 'meteor/reactive-dict';

import { insert } from '../methods/form.js';
import { formSchema, htmlSchema, formKeys } from '../data/formDefinition.js';

import './main.html';

Template.DemoForm.onCreated(function() {
  this.errors = new ReactiveDict();
});

Template.DemoForm.helpers({
  formInputs() {
    return htmlSchema;
  },
  formLabel(name) {
    return formSchema.label(name);
  },
  errors(fieldName) {
    return Template.instance().errors.get(fieldName);
  },
  isValid(fieldName) {
    const errors = Template.instance().errors.get(fieldName);
    return errors && errors.length > 0 ? 'invalid' : '';
  }
});

Template.DemoForm.events({
  'submit .DemoForm' (event) {
    event.preventDefault();

    const instance = Template.instance();

    // Pull form data
    const data = _.reduce(formKeys, (memo, value, key) => {
      memo[value] = event.target[value].value;
      return memo;
    }, {});

    // Attempt to insert via method, with validation on both sides
    insert.call(data, (err, res) => {
      if (err) {
        if (err.error === 'validation-error') {
          // Initialize error object to empty array (i.e. no errors for each input)
          const errors = _.reduce(formKeys, (memo, value, key) => {
            memo[value] = [];
            return memo;
          }, {});

          // Go through validation errors returned from ValidatedMethod
          err.details.forEach((fieldError) => {
            errors[fieldError.name].push(formSchema.label(fieldError.name) + " is invalid");
          });

          // Update ReactiveDict for errors to display
          instance.errors.set(errors);
        }
      } else {
        $('#successModal').openModal();

        // Clear form
        _.each(formKeys, (value) => {
          event.target[value].value = '';
          $("input").removeClass("invalid");
          instance.errors.clear();
        });
      }
    });
  }
});
