This is a simple Meteor app that I wrote in 2 evenings for Practichem which allows HTML forms to be declared in JSON and validated on both the client and server automatically.

I pushed it to Heroku and it can be [viewed here](https://shayne-practichem.herokuapp.com/). Note, it is using free hosting and may need to be spun up after periods of inactivity.