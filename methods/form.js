import { formSchema } from '../data/formDefinition.js';

// Client-side and Server-side validation in one place
export const insert = new ValidatedMethod({
  name: 'FormData.methods.insert',
  validate: formSchema.validator(),
  run(newDoc) {
    // Already validated against schema
    FormData.insert(newDoc)
  }
});
