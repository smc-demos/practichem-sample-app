// This object defines everything required to display and validate a form.
// Validations are handled by simpleSchema props (backed by Simple Schema),
// and HTML customizations are handled by input props overrides 
// (only supports changing type as of now).
const formDefinition = {
  email: {
    simpleSchema: { type: String, regEx: SimpleSchema.RegEx.Email },
    input: { type: 'email' }
  },
  description: {
    simpleSchema: { type: String, min: 5 }
  },
  dollar_amount: {
    simpleSchema: { type: String, regEx: /^-?\d+(\.\d{2})?$/ }
  }
};

function parseSimpleSchema(obj) {
  // Note: Meteor Underscore lib outdated and missing _.mapObject
  return _.reduce(obj, (memo, value, key) => {
    memo[key] = value.simpleSchema;
    return memo;
  }, {});
}
export const formSchema = new SimpleSchema(parseSimpleSchema(formDefinition));

function parseHtmlSchema(obj) {
  return _.reduce(obj, (memo, value, key) => {
    const input = { name: key };
    input.type = value.input ? value.input.type : 'text';
    memo.push(input);
    return memo;
  }, []);
}
export const htmlSchema = parseHtmlSchema(formDefinition);

export const formKeys = Object.keys(formDefinition);
